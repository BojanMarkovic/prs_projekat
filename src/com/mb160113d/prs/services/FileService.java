package com.mb160113d.prs.services;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileService {

    public static void writeToFile(String name, String text) throws IOException {
        File file = new File(name);
        file.getParentFile().mkdir();
        file.createNewFile();
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(text);
        }
    }

}
