package com.mb160113d.prs.services;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.objects.Matrix;
import com.mb160113d.prs.objects.OutputParameters;
import org.ejml.simple.SimpleMatrix;

import java.util.stream.IntStream;

public class AnalyticalService {

    private static final double S_MS = 1000;

    //(Pt-I)*X=constants
    //must divide with mi at the end
    public OutputParameters generateOutput(InputParameters inputParameters) {
        double[][] matrix = inputParameters.getMatrix().getMatrix();
        double[] x = new double[inputParameters.getSize()];
        double[] mi = new double[inputParameters.getSize()];
        //set mi
        mi[Matrix.CPU] = S_MS / inputParameters.getSp();
        mi[Matrix.SYSTEM1] = S_MS / inputParameters.getSd1();
        mi[Matrix.SYSTEM2] = S_MS / inputParameters.getSd2();
        mi[Matrix.SYSTEM3] = S_MS / inputParameters.getSd3();
        for (int i = 4; i < mi.length; i++) {
            mi[i] = S_MS / inputParameters.getSdk();
        }
        //set constants vector
        SimpleMatrix simpleMatrixConstants = new SimpleMatrix(inputParameters.getSize() - 1, 1);
        for (int i = 0; i < (inputParameters.getSize() - 1); i++) {
            simpleMatrixConstants.set(i, matrix[Matrix.CPU][i + 1] * mi[Matrix.CPU]);
        }
        //set matrixP
        SimpleMatrix simpleMatrixP = new SimpleMatrix(inputParameters.getSize() - 1, inputParameters.getSize() - 1);
        for (int i = 0; i < inputParameters.getSize() - 1; i++) {
            for (int j = 0; j < inputParameters.getSize() - 1; j++) {
                simpleMatrixP.set(i, j, matrix[i + 1][j + 1]);
            }
        }
        //setMatrixU
        SimpleMatrix simpleMatrixU = new SimpleMatrix(inputParameters.getSize() - 1, inputParameters.getSize() - 1);
        for (int i = 1; i < mi.length; i++) {
            simpleMatrixU.set(i - 1, i - 1, mi[i]);
        }

        simpleMatrixP = simpleMatrixP.transpose();
        simpleMatrixP = simpleMatrixP.minus(SimpleMatrix.identity(inputParameters.getSize() - 1));
        simpleMatrixP = simpleMatrixP.mult(simpleMatrixU);
        SimpleMatrix solution = simpleMatrixP.solve(simpleMatrixConstants);

        //manipulation to normalise x[Matrix.CPU]
        double sum = IntStream.range(1, mi.length).mapToDouble(i -> matrix[i][Matrix.CPU] * mi[i] * solution.get(i - 1)).sum();
        x[Matrix.CPU] = -sum / mi[Matrix.CPU] / (matrix[Matrix.CPU][Matrix.CPU] - 1);
        for (int i = 1; i < x.length; i++) {
            x[i] = solution.get(i - 1) / x[Matrix.CPU];
        }
        x[Matrix.CPU] = 1;

        return new OutputParameters(x, inputParameters);
    }
}
