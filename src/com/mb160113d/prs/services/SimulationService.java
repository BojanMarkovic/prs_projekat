package com.mb160113d.prs.services;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.objects.Matrix;
import com.mb160113d.prs.objects.OutputParameters;
import com.mb160113d.prs.objects.SimulationServer;

import java.io.IOException;
import java.util.LinkedList;

public class SimulationService {
    public static final double TIME_STEP = 0.1;
    private final SimulationServer[] simulationServers;
    private final InputParameters inputParameters;
    private final String criticalResource;
    private double systemTimeMS;
    private boolean flag;

    public SimulationService(long limit, InputParameters inputParameters) {
        this.systemTimeMS = 0;
        this.inputParameters = inputParameters;
        simulationServers = new SimulationServer[inputParameters.getSize()];
        simulationServers[Matrix.CPU] = new SimulationServer(inputParameters.getSp(),
                this, inputParameters.getMatrix().getMatrix()[Matrix.CPU]);
        simulationServers[Matrix.SYSTEM1] = new SimulationServer(inputParameters.getSd1(),
                this, inputParameters.getMatrix().getMatrix()[Matrix.SYSTEM1]);
        simulationServers[Matrix.SYSTEM2] = new SimulationServer(inputParameters.getSd2(),
                this, inputParameters.getMatrix().getMatrix()[Matrix.SYSTEM2]);
        simulationServers[Matrix.SYSTEM3] = new SimulationServer(inputParameters.getSd3(),
                this, inputParameters.getMatrix().getMatrix()[Matrix.SYSTEM3]);
        for (int i = 0; i < inputParameters.getK(); i++) {
            simulationServers[Matrix.K + i] = new SimulationServer(inputParameters.getSdk(),
                    this, inputParameters.getMatrix().getMatrix()[Matrix.K + i]);
        }

        simulationServers[Matrix.CPU].addJobs(inputParameters.getN());
        flag = true;
        long now = System.currentTimeMillis();
        while (systemTimeMS < limit) {
            if (!flag) {
                for (SimulationServer simulationServer : simulationServers) {
                    simulationServer.calcTimeAvgJobs();
                }
            }
            for (SimulationServer simulationServer : simulationServers) {
                simulationServer.finishJob();
            }
            if (flag) {
                for (SimulationServer simulationServer : simulationServers) {
                    simulationServer.run();
                }
            }
            systemTimeMS += TIME_STEP;
            flag = false;
        }
        double[] tempUsage = new double[simulationServers.length];
        for (int i = 0; i < simulationServers.length; i++) {
            tempUsage[i] = simulationServers[i].getUsage();
        }
        this.criticalResource = MiscService.calculateCriticalResource(tempUsage);

        System.out.println((System.currentTimeMillis() - now) / 1000);
    }

    public double systemResponse() {
        return systemTimeMS * inputParameters.getN() / simulationServers[Matrix.CPU].getSumProcesses() / 100;
    }

    public void sendJob(int type) {
        simulationServers[type].addJob();
    }

    public SimulationServer[] getSimulationServers() {
        return simulationServers;
    }

    public InputParameters getInputParameters() {
        return inputParameters;
    }

    public double getSystemTimeMS() {
        return systemTimeMS;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String writeData() {
        StringBuilder stringBuilder = new StringBuilder();
        for (SimulationServer simulationServer : simulationServers) {
            stringBuilder.append(String.format("%,f        %,f sec-1      %,f        %,f\r\n",
                    simulationServer.getUsage(), simulationServer.getThroughput(), simulationServer.getAverageJobs(), simulationServer.getTimeResponse()));
        }
        return stringBuilder.toString();
    }

    public String writeToFileHeader() {
        return String.format("%1s %20s %20s %20s\r\n", "usage", "throughput", "averageJobs", "timeResponse");
    }

    public String writeToFileCriticalResource() {
        return "Critical resource: " + criticalResource + "\n";
    }

    public String writeTimePropagation() {
        return String.format("%7s %,f\r\n", "TimePropagation=", systemResponse());
    }

    public void writeToFile() throws IOException {
        FileService.writeToFile("output\\result_simulation.txt", writeToFileHeader() +
                writeData() +
                writeTimePropagation() +
                writeToFileCriticalResource());
        LinkedList<LinkedList<Object>> dataToWrite = new LinkedList<>();
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("K= " + inputParameters.getK() + ", N= " + inputParameters.getN());
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("usage");
        dataToWrite.getLast().add("throughput");
        dataToWrite.getLast().add("averageJobs");
        dataToWrite.getLast().add("timeResponse");
        for (SimulationServer simulationServer : simulationServers) {
            dataToWrite.add(new LinkedList<>());
            dataToWrite.getLast().add(simulationServer.getUsage());
            dataToWrite.getLast().add(simulationServer.getThroughput());
            dataToWrite.getLast().add(simulationServer.getAverageJobs());
            dataToWrite.getLast().add(simulationServer.getTimeResponse());
        }

        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("TimePropagation=");
        dataToWrite.getLast().add(systemResponse());
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("Critical resource: ");
        dataToWrite.getLast().add(criticalResource);

        ExelService exelService = new ExelService();
        exelService.writeToExel("output\\result_simulation.xlsx", dataToWrite);
    }

    public OutputParameters makeOutputParameters() {
        OutputParameters outputParameters = new OutputParameters(systemResponse());
        double[] tempUsage = new double[simulationServers.length];
        double[] tempThroughtput = new double[simulationServers.length];
        double[] tempAverageJobs = new double[simulationServers.length];
        double[] tempTimeResponse = new double[simulationServers.length];
        int bound = simulationServers.length;
        for (int i = 0; i < bound; i++) {
            tempUsage[i] = simulationServers[i].getUsage();
            tempThroughtput[i] = simulationServers[i].getThroughput();
            tempAverageJobs[i] = simulationServers[i].getAverageJobs();
            tempTimeResponse[i] = simulationServers[i].getTimeResponse();
        }
        outputParameters.setUsage(tempUsage);
        outputParameters.setThroughput(tempThroughtput);
        outputParameters.setAverageJobs(tempAverageJobs);
        outputParameters.setTimeResponse(tempTimeResponse);
        outputParameters.calculateCriticalResource();

        return outputParameters;
    }
}
