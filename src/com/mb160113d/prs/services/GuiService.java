package com.mb160113d.prs.services;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class GuiService {

    private static final int LABEL_WIDTH = 90;
    private static final int BUTTON_WIDTH = 200;
    private static final int FIELD_WIDTH = 70;
    private static final int GAP = 10;
    private static final int PROGRESS_INDICATOR_WIDTH = 400;
    private static final int PROGRESS_INDICATOR_HEIGHT = 200;
    private static final int GRID_GAP = 20;

    public Label generateLabel(String text) {
        Label temp = new Label(text);
        temp.setMinWidth(LABEL_WIDTH);
        return temp;
    }

    public TextField generateTextField(String text) {
        TextField temp = new TextField(text);
        temp.setMinWidth(FIELD_WIDTH);
        return temp;
    }

    public Button generateButton(String text) {
        Button temp = new Button(text);
        temp.setMinWidth(BUTTON_WIDTH);
        return temp;
    }

    public void setStage(Stage stage, String title) {
        stage.setTitle(title);
        stage.setResizable(false);
    }

    public GridPane generateGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setHgap(GRID_GAP);
        gridPane.setVgap(GRID_GAP);
        return gridPane;
    }

    public FlowPane generateFlowPane() {
        FlowPane flowPane = new FlowPane();

        flowPane.setPadding(new Insets(GAP, GAP, GAP, GAP));
        flowPane.setVgap(GAP);
        flowPane.setHgap(GAP);
        flowPane.setAlignment(Pos.BASELINE_CENTER);
        flowPane.setOrientation(Orientation.HORIZONTAL);
        return flowPane;
    }

    public ProgressIndicator getProgressIndicator(Stage stage) {
        Label progressLabel = new Label("Please wait");
        progressLabel.setAlignment(Pos.CENTER);
        ProgressIndicator progressIndicator = new ProgressIndicator();
        FlowPane flowPaneProgress = generateFlowPane();
        flowPaneProgress.getChildren().addAll(progressLabel, progressIndicator);

        Scene progressScene = new Scene(flowPaneProgress, PROGRESS_INDICATOR_WIDTH, PROGRESS_INDICATOR_HEIGHT);
        stage.setScene(progressScene);
        return progressIndicator;
    }

    public void generateAlertWindow(Alert.AlertType type, String header, String content) {
        Alert alert = new Alert(type);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
}
