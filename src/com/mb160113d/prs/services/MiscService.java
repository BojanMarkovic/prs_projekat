package com.mb160113d.prs.services;

import com.mb160113d.prs.gui.WelcomeWindow;
import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.objects.Matrix;
import com.mb160113d.prs.objects.OutputParameters;
import com.mb160113d.prs.objects.TextFieldSpeeds;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MiscService {

    public static String calculateCriticalResource(double[] usage) {
        double max = usage[0];
        for (int i = 1; i < usage.length; i++) {
            if (usage[i] > max) {
                max = usage[i];
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < usage.length; i++) {
            if (usage[i] == max) {
                switch (i) {
                    case Matrix.CPU:
                        stringBuilder.append("CPU\t");
                        break;
                    case Matrix.SYSTEM1:
                        stringBuilder.append("System disc 1\t");
                        break;
                    case Matrix.SYSTEM2:
                        stringBuilder.append("System disc 2\t");
                        break;
                    case Matrix.SYSTEM3:
                        stringBuilder.append("System disc 3\t");
                        break;
                    default:
                        stringBuilder.append("User disc ").append(i - 4).append("\t");
                        break;
                }
            }
        }
        return stringBuilder.toString();
    }

    public static GridPane generateSpeedInput(HBox buttonsHBox, TextFieldSpeeds textFieldSpeeds,
                                              InputParameters inputParameters, boolean showMultiProgramming) {
        GuiService guiService = new GuiService();
        int row = 0;
        final int COLUMN_STRING = 0;
        final int COLUMN_FIELD = 1;
        final int COLUMN_UNITS = 2;
        GridPane gridPane = new GridPane();
        TextField cpuSpeed = guiService.generateTextField(String.valueOf(inputParameters.getSp()));
        textFieldSpeeds.setCpuSpeed(cpuSpeed);
        gridPane.add(guiService.generateLabel("Cpu speed: "), COLUMN_STRING, row);
        gridPane.add(cpuSpeed, COLUMN_FIELD, row);
        gridPane.add(guiService.generateLabel("ms"), COLUMN_UNITS, row++);

        TextField system1Speed = guiService.generateTextField(String.valueOf(inputParameters.getSd1()));
        textFieldSpeeds.setSystem1Speed(system1Speed);
        gridPane.add(guiService.generateLabel("System 1 speed: "), COLUMN_STRING, row);
        gridPane.add(system1Speed, COLUMN_FIELD, row);
        gridPane.add(guiService.generateLabel("ms"), COLUMN_UNITS, row++);

        TextField system2Speed = guiService.generateTextField(String.valueOf(inputParameters.getSd2()));
        textFieldSpeeds.setSystem2Speed(system2Speed);
        gridPane.add(guiService.generateLabel("System 2 speed: "), COLUMN_STRING, row);
        gridPane.add(system2Speed, COLUMN_FIELD, row);
        gridPane.add(guiService.generateLabel("ms"), COLUMN_UNITS, row++);

        TextField system3Speed = guiService.generateTextField(String.valueOf(inputParameters.getSd3()));
        textFieldSpeeds.setSystem3Speed(system3Speed);
        gridPane.add(guiService.generateLabel("System 3 speed: "), COLUMN_STRING, row);
        gridPane.add(system3Speed, COLUMN_FIELD, row);
        gridPane.add(guiService.generateLabel("ms"), COLUMN_UNITS, row++);

        TextField userKSpeed = guiService.generateTextField(String.valueOf(inputParameters.getSdk()));
        textFieldSpeeds.setUserKSpeed(userKSpeed);
        gridPane.add(guiService.generateLabel("User disc speed: "), COLUMN_STRING, row);
        gridPane.add(userKSpeed, COLUMN_FIELD, row);
        gridPane.add(guiService.generateLabel("ms"), COLUMN_UNITS, row++);

        if (showMultiProgramming) {
            TextField multiProgramming = guiService.generateTextField(String.valueOf(inputParameters.getN()));
            textFieldSpeeds.setMultiProgramming(multiProgramming);
            gridPane.add(guiService.generateLabel("Multiprogramming level: "), COLUMN_STRING, row);
            gridPane.add(multiProgramming, COLUMN_FIELD, row++);
        }

        gridPane.add(buttonsHBox, 0, row, 2, 1);
        return gridPane;
    }

    public static GridPane generateTableForData(InputParameters inputParameters, OutputParameters outputParameters,
                                                Stage stage, boolean showTimeResponseAndX) {
        GuiService guiService = new GuiService();
        GridPane gridPane = guiService.generateGridPane();
        int row = 0;
        int column = 1;
        if (showTimeResponseAndX) {
            gridPane.add(guiService.generateLabel("x"), column++, row);
        }
        gridPane.add(guiService.generateLabel("usage"), column++, row);
        gridPane.add(guiService.generateLabel("throughput sec-1"), column++, row);
        gridPane.add(guiService.generateLabel("averageJobs"), column++, row);
        gridPane.add(guiService.generateLabel("timeResponse"), column, row++);
        gridPane.add(guiService.generateLabel("Cpu: "), 0, row++);
        gridPane.add(guiService.generateLabel("System 1: "), 0, row++);
        gridPane.add(guiService.generateLabel("System 2: "), 0, row++);
        gridPane.add(guiService.generateLabel("System 3: "), 0, row++);
        for (int j = 0; j < inputParameters.getK(); j++) {
            gridPane.add(guiService.generateLabel("User disc " + j + ": "), 0, row++);
        }
        row = 1;
        column = 1;
        for (int i = 0; i < inputParameters.getSize(); i++) {
            if (showTimeResponseAndX) {
                gridPane.add(guiService.generateLabel(String.valueOf(outputParameters.getX()[i])), column++, row);
            }
            gridPane.add(guiService.generateLabel(String.valueOf(outputParameters.getUsage()[i])), column++, row);
            gridPane.add(guiService.generateLabel(String.valueOf(outputParameters.getThroughput()[i])), column++, row);
            gridPane.add(guiService.generateLabel(String.valueOf(outputParameters.getAverageJobs()[i])), column++, row);

            gridPane.add(guiService.generateLabel(String.valueOf(outputParameters.getTimeResponse()[i])), column, row++);

            column = 1;
        }
        gridPane.add(guiService.generateLabel("Time of propagation"), 0, row);
        gridPane.add(guiService.generateLabel(outputParameters.getTimePropagation() + " sec"), column, row++);
        gridPane.add(guiService.generateLabel("Critical resource "), 0, row);
        gridPane.add(guiService.generateLabel(outputParameters.getCriticalResource()), column, row++);
        Button mainMenu = guiService.generateButton("Return to main menu");
        mainMenu.setOnAction(event -> {
            new WelcomeWindow();
            stage.close();
        });
        gridPane.add(mainMenu, 0, row, 2, 1);
        return gridPane;
    }
}
