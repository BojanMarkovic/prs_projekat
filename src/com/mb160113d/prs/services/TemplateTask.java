package com.mb160113d.prs.services;

import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.util.concurrent.Callable;

public class TemplateTask extends Task<Void> {
    private final Scene scene;
    private final Stage stage;
    private final GuiService guiService;
    private final Callable<Void> callable;
    private final Callable<Void> callableOnSucceeded;

    public TemplateTask(Scene scene, Stage stage, Callable<Void> callable, Callable<Void> callableOnSucceeded) {
        this.scene = scene;
        this.stage = stage;
        this.guiService = new GuiService();
        this.callable = callable;
        this.callableOnSucceeded = callableOnSucceeded;
    }

    @Override
    protected Void call() throws Exception {
        callable.call();
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        guiService.generateAlertWindow(Alert.AlertType.INFORMATION, "Info", "Process finished");
        stage.close();
        try {
            callableOnSucceeded.call();
        } catch (Exception e) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Error", "Something happened\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void failed() {
        super.failed();
        stage.setScene(scene);
        Throwable throwable = this.getException();
        if (throwable instanceof Exception) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Error", "Something happened\n" + throwable.getMessage());
            throwable.printStackTrace();
        }
    }
}
