package com.mb160113d.prs.services;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.util.LinkedList;

public class ExelService {
    public void writeToExel(String nameFile, LinkedList<LinkedList<Object>> data) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Rezultati");

        int rowCount = 0;

        for (LinkedList list : data) {
            Row row = sheet.createRow(++rowCount);

            int columnCount = 0;

            for (Object field : list) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double) {
                    cell.setCellValue((Double) field);
                }
            }
        }
        try (FileOutputStream outputStream = new FileOutputStream(nameFile)) {
            workbook.write(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exel error");
        }
    }
}
