package com.mb160113d.prs.objects;

import javafx.scene.control.TextField;

public class TextFieldSpeeds {
    private TextField cpuSpeed;
    private TextField system1Speed;
    private TextField system2Speed;
    private TextField system3Speed;
    private TextField userKSpeed;
    private TextField multiProgramming;

    public TextField getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuSpeed(TextField cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public TextField getSystem1Speed() {
        return system1Speed;
    }

    public void setSystem1Speed(TextField system1Speed) {
        this.system1Speed = system1Speed;
    }

    public TextField getSystem2Speed() {
        return system2Speed;
    }

    public void setSystem2Speed(TextField system2Speed) {
        this.system2Speed = system2Speed;
    }

    public TextField getSystem3Speed() {
        return system3Speed;
    }

    public void setSystem3Speed(TextField system3Speed) {
        this.system3Speed = system3Speed;
    }

    public TextField getUserKSpeed() {
        return userKSpeed;
    }

    public void setUserKSpeed(TextField userKSpeed) {
        this.userKSpeed = userKSpeed;
    }

    public TextField getMultiProgramming() {
        return multiProgramming;
    }

    public void setMultiProgramming(TextField multiProgramming) {
        this.multiProgramming = multiProgramming;
    }
}
