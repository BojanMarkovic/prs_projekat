package com.mb160113d.prs.objects;

import com.mb160113d.prs.services.ExelService;
import com.mb160113d.prs.services.FileService;
import com.mb160113d.prs.services.MiscService;

import java.io.IOException;
import java.util.LinkedList;

public class OutputParameters {
    private static final int CONVERT_MS_S = 1000;
    private final double[] x;
    private final double timePropagation;
    private InputParameters inputParameters;
    private String criticalResource;
    private double[] gordonNewel;
    private double[] usage;
    private double[] throughput;
    private double[] averageJobs;
    private double[] timeResponse;

    public OutputParameters(double[] x, InputParameters inputParameters) {
        this.x = x;
        this.inputParameters = inputParameters;
        gordonNewel(inputParameters.getN());
        calculateUsage(inputParameters.getSize());
        calculateThroughput(inputParameters);
        calculateAverageJobs(inputParameters);
        double productivity = throughput[Matrix.CPU] * inputParameters.getMatrix().getMatrix()[Matrix.CPU][Matrix.CPU];
        this.timePropagation = inputParameters.getN() / productivity;
        calculateTimeResponse(inputParameters.getSize());
        this.criticalResource = MiscService.calculateCriticalResource(usage);
    }

    public OutputParameters(double timePropagation) {
        this.x = null;
        this.timePropagation = timePropagation;
    }

    public void calculateCriticalResource() {
        this.criticalResource = MiscService.calculateCriticalResource(usage);
    }

    private void calculateTimeResponse(int size) {
        timeResponse = new double[size];
        for (int i = 0; i < size; i++) {
            timeResponse[i] = averageJobs[i] / throughput[i];
        }
    }

    private void calculateAverageJobs(InputParameters inputParameters) {
        averageJobs = new double[inputParameters.getSize()];
        for (int i = 0; i < averageJobs.length; i++) {
            for (int j = 1; j < inputParameters.getN() + 1; j++) {
                averageJobs[i] += Math.pow(x[i], j) * gordonNewel[gordonNewel.length - 1 - j];
            }
            averageJobs[i] /= gordonNewel[gordonNewel.length - 1];
        }
    }

    private void calculateThroughput(InputParameters inputParameters) {
        throughput = new double[inputParameters.getSize()];
        throughput[Matrix.CPU] = CONVERT_MS_S * usage[Matrix.CPU] / inputParameters.getSp();
        throughput[Matrix.SYSTEM1] = CONVERT_MS_S * usage[Matrix.SYSTEM1] / inputParameters.getSd1();
        throughput[Matrix.SYSTEM2] = CONVERT_MS_S * usage[Matrix.SYSTEM2] / inputParameters.getSd2();
        throughput[Matrix.SYSTEM3] = CONVERT_MS_S * usage[Matrix.SYSTEM3] / inputParameters.getSd3();
        for (int i = 0; i < inputParameters.getK(); i++) {
            throughput[Matrix.K + i] = CONVERT_MS_S * usage[Matrix.K + i] / inputParameters.getSdk();
        }
    }

    private void calculateUsage(int size) {
        usage = new double[size];
        double temp = gordonNewel[gordonNewel.length - 2] / gordonNewel[gordonNewel.length - 1];
        for (int i = 0; i < usage.length; i++) {
            usage[i] = temp * x[i];
        }
    }

    private void gordonNewel(int n) {
        gordonNewel = new double[n + 1];
        gordonNewel[0] = 1;
        for (double v : x) {
            for (int j = 1; j < (n + 1); j++) {
                gordonNewel[j] += v * gordonNewel[j - 1];
            }
        }
    }

    public double[] getTimeResponse() {
        return timeResponse;
    }

    public void setTimeResponse(double[] timeResponse) {
        this.timeResponse = timeResponse;
    }

    public double[] getX() {
        return x;
    }

    public double[] getUsage() {
        return usage;
    }

    public void setUsage(double[] usage) {
        this.usage = usage;
    }

    public double[] getThroughput() {
        return throughput;
    }

    public void setThroughput(double[] throughput) {
        this.throughput = throughput;
    }

    public double[] getAverageJobs() {
        return averageJobs;
    }

    public void setAverageJobs(double[] averageJobs) {
        this.averageJobs = averageJobs;
    }

    public double getTimePropagation() {
        return timePropagation;
    }

    public String getCriticalResource() {
        return criticalResource;
    }

    public String writeTimePropagation() {
        return String.format("%7s %,f sec\r\n", "TimePropagation=", timePropagation);
    }

    public String writeToFileHeader() {
        return String.format("%1s %20s %20s %20s %20s \r\n", "x", "usage", "throughput", "averageJobs", "timeResponse");
    }

    public String writeData() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < x.length; i++) {
            stringBuilder.append(String.format("%,f         %,f        %,f sec-1      %,f            %,f\r\n",
                    x[i], usage[i], throughput[i], averageJobs[i], timeResponse[i]));
        }
        return stringBuilder.toString();
    }

    public String writeDataWithoutX() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < x.length; i++) {
            stringBuilder.append(String.format("%,f        %,f sec-1      %,f            %,f\r\n",
                    usage[i], throughput[i], averageJobs[i], timeResponse[i]));
        }
        return stringBuilder.toString();
    }

    public String writeToFileHeaderWithoutX() {
        return String.format("%1s %20s %20s %20s \r\n", "usage", "throughput", "averageJobs", "timeResponse");
    }

    public String writeToFileCriticalResource() {
        return "Critical resource: " + criticalResource + "\n";
    }

    public void writeToFile() throws IOException {
        FileService.writeToFile("output\\result_analytical.txt", writeToFileHeader() +
                writeData() +
                writeTimePropagation() +
                writeToFileCriticalResource());

        LinkedList<LinkedList<Object>> dataToWrite = new LinkedList<>();
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("K= " + inputParameters.getK() + ", N= " + inputParameters.getN());
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("usage");
        dataToWrite.getLast().add("throughput");
        dataToWrite.getLast().add("averageJobs");
        for (int i = 0; i < usage.length; i++) {
            dataToWrite.add(new LinkedList<>());
            dataToWrite.getLast().add(usage[i]);
            dataToWrite.getLast().add(throughput[i]);
            dataToWrite.getLast().add(averageJobs[i]);
        }

        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("TimePropagation=");
        dataToWrite.getLast().add(timePropagation);
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("Critical resource: ");
        dataToWrite.getLast().add(criticalResource);

        ExelService exelService = new ExelService();
        exelService.writeToExel("output\\result_analytical.xlsx", dataToWrite);
    }
}
