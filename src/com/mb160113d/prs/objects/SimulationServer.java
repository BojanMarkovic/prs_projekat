package com.mb160113d.prs.objects;

import com.mb160113d.prs.services.SimulationService;

import java.security.SecureRandom;
import java.util.LinkedList;

public class SimulationServer {
    private static final int CONVERT_MS_S = 1000;
    private final double averageSpeed;
    private final LinkedList<Integer> waitingQueue;
    private final SecureRandom random;
    private final SimulationService simulationService;
    private final double[] probability;
    private long sumProcesses;
    private double sumTime;
    private double reserved;
    private final LinkedList<Double> timeOfAvgJobs;

    public SimulationServer(int averageSpeed, SimulationService simulationService, double[] probability) {
        this.averageSpeed = averageSpeed;
        this.waitingQueue = new LinkedList<>();
        this.sumProcesses = 0;
        this.sumTime = 0;
        this.random = new SecureRandom();
        this.reserved = -1;
        this.simulationService = simulationService;
        this.probability = new double[probability.length];
        this.timeOfAvgJobs = new LinkedList<>();
        for (int i = 0; i < simulationService.getInputParameters().getN() + 1; i++) {
            this.timeOfAvgJobs.add(0.0);
        }
        double sum = 0;
        for (int i = 0; i < probability.length; i++) {
            this.probability[i] = sum + probability[i];
            sum += probability[i];
        }
    }

    public double getUsage() {
        return sumTime / simulationService.getSystemTimeMS();
    }

    public long getSumProcesses() {
        return sumProcesses;
    }

    public double getTimeResponse() {
        return getAverageJobs() / getThroughput();
    }

    public double getThroughput() {
        return CONVERT_MS_S * sumProcesses / simulationService.getSystemTimeMS();
    }

    //public double getAverageJobs() { return sumTimeQueueAndServer / simulationService.getSystemTimeMS(); }

    public double getAverageJobs() {
        double temp = 0;
        for (int i = 1; i < timeOfAvgJobs.size(); i++) {
            temp += timeOfAvgJobs.get(i) * i / simulationService.getSystemTimeMS();
        }
        return temp;
    }

    public void calcTimeAvgJobs() {
        int numOfJobsOnServer = waitingQueue.size();
        if (reserved > 0) {
            numOfJobsOnServer += 1;
        }
        timeOfAvgJobs.set(numOfJobsOnServer, timeOfAvgJobs.get(numOfJobsOnServer) + SimulationService.TIME_STEP);
    }

    public void finishJob() {
        if (reserved > 0) {
            if (simulationService.getSystemTimeMS() >= reserved) {
                sendJob();
                reserved = -1;
                simulationService.setFlag(true);
            }
        }
    }

    public void addJobs(int i) {
        for (int j = 0; j < i; j++) {
            waitingQueue.add(i);
        }
    }

    public void addJob() {
        waitingQueue.add(1);
    }

    /*private int getRandom() {//https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
        double L = Math.exp(-averageSpeed);
        int k = 0;
        double p = 1.0;
        do {
            p = p * random.nextDouble();
            k++;
        } while (p > L);
        return k - 1;
    }*/

    private double getRandomFast() {
        return -averageSpeed * Math.log(random.nextDouble());
    }

    private void sendJob() {
        double temp = random.nextDouble();
        for (int i = 0; i < probability.length; i++) {
            if (temp < probability[i]) {
                simulationService.sendJob(i);
                return;
            }
        }
        for (int i = 0; i < probability.length; i++) {
            if (probability[i] > 0) {
                simulationService.sendJob(i);
                return;
            }
        }
    }

    public void run() {
        if (reserved < 0) {
            if (!waitingQueue.isEmpty()) {
                waitingQueue.remove();
                double time = getRandomFast();
                reserved = simulationService.getSystemTimeMS() + time;
                sumTime += time;
                sumProcesses += 1;
            }
        }
    }
}
