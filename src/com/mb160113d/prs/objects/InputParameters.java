package com.mb160113d.prs.objects;

public class InputParameters {
    private Matrix matrix;
    private int n;
    private int k;
    private int sp;
    private int sd1;
    private int sd2;
    private int sd3;
    private int sdk;
    private int size;

    public InputParameters() {
        this.n = 10;
        this.k = 2;
        this.sp = 5;
        this.sd1 = 20;
        this.sd2 = 15;
        this.sd3 = 15;
        this.sdk = 20;
        this.size = 4 + this.k;
        this.matrix = new Matrix(k);
    }

    public int getSize() {
        return size;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
        this.matrix = new Matrix(k);
        this.size = 4 + k;
    }

    public int getSp() {
        return sp;
    }

    public int getSd1() {
        return sd1;
    }

    public int getSd2() {
        return sd2;
    }

    public int getSd3() {
        return sd3;
    }

    public int getSdk() {
        return sdk;
    }

    public void setSpeeds(TextFieldSpeeds textFieldSpeeds) {
        this.sd1 = Integer.parseInt(textFieldSpeeds.getSystem1Speed().getText());
        this.sd2 = Integer.parseInt(textFieldSpeeds.getSystem2Speed().getText());
        this.sd3 = Integer.parseInt(textFieldSpeeds.getSystem3Speed().getText());
        this.sdk = Integer.parseInt(textFieldSpeeds.getUserKSpeed().getText());
        this.sp = Integer.parseInt(textFieldSpeeds.getCpuSpeed().getText());
        if (textFieldSpeeds.getMultiProgramming() != null) {
            this.n = Integer.parseInt(textFieldSpeeds.getMultiProgramming().getText());
        }
    }
}
