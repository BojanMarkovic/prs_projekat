package com.mb160113d.prs.objects;

public class Matrix {
    public static final int CPU = 0;
    public static final int SYSTEM1 = 1;
    public static final int SYSTEM2 = 2;
    public static final int SYSTEM3 = 3;
    public static final int K = 4;

    private final double[][] matrix;

    public Matrix(int k) {
        int size = 4 + k;
        double probabilityK = 0.6 / k;
        this.matrix = new double[size][size];
        this.matrix[CPU][SYSTEM1] = 0.1;
        this.matrix[CPU][SYSTEM2] = 0.1;
        this.matrix[CPU][SYSTEM3] = 0.1;
        for (int i = 0; i < k; i++) {
            this.matrix[CPU][K + i] = probabilityK;
            this.matrix[K + i][CPU] = 1;
        }
        this.matrix[CPU][CPU] = 0.1;
        for (int i = 1; i < 4; i++) {
            this.matrix[i][CPU] = 0.4;
            for (int j = 0; j < k; j++) {
                this.matrix[i][K + j] = probabilityK;
            }
        }
    }

    public Matrix(double[][] matrix) {
        this.matrix = matrix;
    }

    public double[][] getMatrix() {
        return matrix;
    }
}
