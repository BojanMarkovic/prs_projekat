package com.mb160113d.prs.exceptions;

public class InputException extends Exception {
    public InputException(String message) {
        super(message);
    }
}
