package com.mb160113d.prs.gui;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.objects.OutputParameters;
import com.mb160113d.prs.objects.TextFieldSpeeds;
import com.mb160113d.prs.services.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.LinkedList;

public class SpeedWindow extends Stage {
    private final InputParameters inputParameters;
    private final GuiService guiService;
    private final Scene scene;
    private final StringBuilder stringBuilderToFile1;
    private final StringBuilder stringBuilderToFile2;
    private final TextFieldSpeeds textFieldSpeeds;
    private final LinkedList<LinkedList<Object>> dataToWrite1;
    private final LinkedList<LinkedList<Object>> dataToWrite2;

    public SpeedWindow() {
        this.stringBuilderToFile1 = new StringBuilder();
        this.stringBuilderToFile2 = new StringBuilder();
        this.inputParameters = new InputParameters();
        this.guiService = new GuiService();
        this.textFieldSpeeds = new TextFieldSpeeds();
        this.dataToWrite1 = new LinkedList<>();
        this.dataToWrite2 = new LinkedList<>();
        guiService.setStage(this, "Welcome");

        FlowPane flowPane = guiService.generateFlowPane();

        Button analyticalMethod = guiService.generateButton("Analytical method");
        analyticalMethod.setOnAction(event -> analyticalMethodOnAction());

        Button back = guiService.generateButton("Back");
        back.setOnAction(event -> {
            new WelcomeWindow();
            this.close();
        });
        HBox buttonsHBox = new HBox(analyticalMethod, back);

        GridPane gridPaneHBox = MiscService.generateSpeedInput(buttonsHBox, textFieldSpeeds, inputParameters, false);


        flowPane.getChildren().addAll(gridPaneHBox);
        this.scene = new Scene(flowPane);
        this.setScene(scene);
        this.show();
    }

    private void analyticalMethodOnAction() {
        inputParameters.setSpeeds(textFieldSpeeds);
        TemplateTask templateTask = new TemplateTask(scene, this, () -> {
            for (int k = 2; k < 9; k++) {

                inputParameters.setK(k);
                inputParameters.setN(10);
                OutputParameters outputParameters = (new AnalyticalService()).generateOutput(inputParameters);
                prepareForFile(outputParameters, inputParameters);
            }
            for (int k = 2; k < 9; k++) {
                inputParameters.setK(k);
                inputParameters.setN(15);
                OutputParameters outputParameters = (new AnalyticalService()).generateOutput(inputParameters);
                prepareForFile(outputParameters, inputParameters);
            }
            for (int k = 2; k < 9; k++) {
                inputParameters.setK(k);
                inputParameters.setN(20);
                OutputParameters outputParameters = (new AnalyticalService()).generateOutput(inputParameters);
                prepareForFile(outputParameters, inputParameters);
            }
            writeToFile();
            return null;
        }, () -> {
            new WelcomeWindow();
            return null;
        });
        guiService.getProgressIndicator(this).progressProperty().bind(templateTask.progressProperty());
        Thread thread = new Thread(templateTask);
        thread.setDaemon(true);
        thread.start();
    }

    private void writeToFile() {
        try {
            FileService.writeToFile("output\\rezultati_analiticki.txt", stringBuilderToFile1.toString());
            FileService.writeToFile("output\\potraznje_analiticki.txt", stringBuilderToFile2.toString());
            ExelService exelService = new ExelService();
            exelService.writeToExel("output\\rezultati_analiticki.xlsx", dataToWrite1);
            ExelService exelService2 = new ExelService();
            exelService2.writeToExel("output\\potraznje_analiticki.xlsx", dataToWrite2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void prepareForFile(OutputParameters outputParameters, InputParameters inputParameters) {
        String tempStart = "********************* K= " + inputParameters.getK() + " ************************* N= "
                + inputParameters.getN() + " *************************\r\n";
        stringBuilderToFile1.append(tempStart);
        stringBuilderToFile2.append(tempStart);
        stringBuilderToFile1.append(outputParameters.writeToFileHeaderWithoutX());
        stringBuilderToFile1.append(outputParameters.writeDataWithoutX());
        stringBuilderToFile1.append(outputParameters.writeTimePropagation());
        stringBuilderToFile1.append(outputParameters.writeToFileCriticalResource());

        stringBuilderToFile2.append(String.format("%1s\r\n", "x"));
        for (double x : outputParameters.getX()) {
            stringBuilderToFile2.append(String.format("%,f\r\n", x));
        }

        dataToWrite1.add(new LinkedList<>());
        dataToWrite1.getLast().add("K= " + inputParameters.getK() + ", N= " + inputParameters.getN());
        dataToWrite1.add(new LinkedList<>());
        dataToWrite1.getLast().add("usage");
        dataToWrite1.getLast().add("throughput");
        dataToWrite1.getLast().add("averageJobs");
        dataToWrite1.getLast().add("timeResponse");

        for (int i = 0; i < outputParameters.getUsage().length; i++) {
            dataToWrite1.add(new LinkedList<>());
            dataToWrite1.getLast().add(outputParameters.getUsage()[i]);
            dataToWrite1.getLast().add(outputParameters.getThroughput()[i]);
            dataToWrite1.getLast().add(outputParameters.getAverageJobs()[i]);
            dataToWrite1.getLast().add(outputParameters.getTimeResponse()[i]);
        }

        dataToWrite1.add(new LinkedList<>());
        dataToWrite1.getLast().add("TimePropagation=");
        dataToWrite1.getLast().add(outputParameters.getTimePropagation());
        dataToWrite1.add(new LinkedList<>());
        dataToWrite1.getLast().add("Critical resource: ");
        dataToWrite1.getLast().add(outputParameters.getCriticalResource());

        dataToWrite2.add(new LinkedList<>());
        dataToWrite2.getLast().add("K= " + inputParameters.getK() + ", N= " + inputParameters.getN());
        dataToWrite2.add(new LinkedList<>());
        dataToWrite2.getLast().add("X");

        for (int i = 0; i < outputParameters.getX().length; i++) {
            dataToWrite2.add(new LinkedList<>());
            dataToWrite2.getLast().add(outputParameters.getX()[i]);
        }
    }
}
