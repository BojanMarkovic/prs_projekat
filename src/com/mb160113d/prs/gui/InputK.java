package com.mb160113d.prs.gui;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.services.GuiService;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class InputK extends Stage {
    private final InputParameters inputParameters;

    public InputK(InputParameters inputParameters) {
        this.inputParameters = inputParameters;
        GuiService guiService = new GuiService();
        guiService.setStage(this, "Manual input");

        FlowPane flowPane = guiService.generateFlowPane();

        TextField textFieldUserK = guiService.generateTextField("2");
        HBox hBoxUserK = new HBox(guiService.generateLabel("User disc K:   "), textFieldUserK);
        Button chooseKSize = guiService.generateButton("Choose K value");
        chooseKSize.setOnAction(event -> {
            try {
                this.inputParameters.setK(Integer.parseInt(textFieldUserK.getText()));
                this.close();
                new InputWindow(this.inputParameters);
            } catch (NumberFormatException exception) {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                errorAlert.setHeaderText("Input not valid");
                errorAlert.setContentText("It must be number");
                errorAlert.showAndWait();
            }
        });

        Button back = guiService.generateButton("Back");
        back.setOnAction(event -> {
            new WelcomeWindow();
            this.close();
        });

        flowPane.getChildren().addAll(hBoxUserK, chooseKSize, back);
        this.setScene(new Scene(flowPane));
        this.show();
    }
}
