package com.mb160113d.prs.gui;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.services.*;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.LinkedList;

public class TimeSelection extends Stage {
    private final InputParameters inputParameters;
    private final GuiService guiService;
    private final Scene scene;
    private final StringBuilder stringBuilderToFile;
    private final LinkedList<LinkedList<Object>> dataToWrite;
    private final LinkedList<LinkedList<Object>> dataToWrite2;
    private final StringBuilder stringBuilderIterationToFile;
    private SimulationService simulationService;
    private LinkedList<SimulationService> simulationServiceLinkedList;

    public TimeSelection(InputParameters inputParameters, boolean manualInput) {
        this.stringBuilderToFile = new StringBuilder();
        this.dataToWrite = new LinkedList<>();
        this.dataToWrite2 = new LinkedList<>();
        this.inputParameters = inputParameters;
        this.stringBuilderIterationToFile = new StringBuilder();
        this.guiService = new GuiService();
        guiService.setStage(this, "Time selection");

        FlowPane flowPane = guiService.generateFlowPane();

        TextField timeField = new TextField("86400000");
        Button simulate = new Button("Simulate");
        simulate.setOnAction(event -> {
            if (manualInput) simulateOnActionManualInput(timeField);
            else simulateOnAction(timeField);
        });

        Button back = guiService.generateButton("Back");
        back.setOnAction(event -> {
            if (manualInput) {
                new InputWindow(inputParameters);
            } else {
                new WelcomeWindow();
            }
            this.close();
        });

        flowPane.getChildren().addAll(timeField, simulate, back);
        this.scene = new Scene(flowPane);
        this.setScene(scene);
        this.show();
    }

    private void simulateOnAction(TextField timeField) {
        TemplateTask templateTask = new TemplateTask(scene, this, () -> {
            for (int k = 2; k < 9; k++) {
                simulationServiceLinkedList = new LinkedList<>();
                for (int i = 0; i < 10; i++) {
                    inputParameters.setK(k);
                    inputParameters.setN(10);
                    simulationServiceLinkedList.add(new SimulationService(Long.parseLong(timeField.getText()), inputParameters));
                }
                prepareForFile(simulationServiceLinkedList);
                simulationServiceLinkedList.clear();
            }

            for (int k = 2; k < 9; k++) {
                simulationServiceLinkedList = new LinkedList<>();
                for (int i = 0; i < 25; i++) {
                    inputParameters.setK(k);
                    inputParameters.setN(15);
                    simulationServiceLinkedList.add(new SimulationService(Long.parseLong(timeField.getText()), inputParameters));
                }
                prepareForFile(simulationServiceLinkedList);
                simulationServiceLinkedList.clear();
            }

            for (int k = 2; k < 9; k++) {
                simulationServiceLinkedList = new LinkedList<>();
                for (int i = 0; i < 100; i++) {
                    inputParameters.setK(k);
                    inputParameters.setN(20);
                    simulationServiceLinkedList.add(new SimulationService(Long.parseLong(timeField.getText()), inputParameters));
                }
                prepareForFile(simulationServiceLinkedList);
                simulationServiceLinkedList.clear();
            }
            writeToFile();
            return null;
        }, () -> {
            new WelcomeWindow();
            return null;
        });
        guiService.getProgressIndicator(this).progressProperty().bind(templateTask.progressProperty());
        Thread thread = new Thread(templateTask);
        thread.setDaemon(true);
        thread.start();
    }

    private void simulateOnActionManualInput(TextField timeField) {
        TemplateTask templateTask = new TemplateTask(scene, this, () -> {
            simulationService = new SimulationService(Long.parseLong(timeField.getText()), inputParameters);
            return null;
        }, () -> {
            new SimulatedWindow(simulationService);
            return null;
        });
        guiService.getProgressIndicator(this).progressProperty().bind(templateTask.progressProperty());
        Thread thread = new Thread(templateTask);
        thread.setDaemon(true);
        thread.start();
    }

    private void writeToFile() {
        try {
            FileService.writeToFile("output\\rezultati_simulacija_usrednjeno.txt", stringBuilderToFile.toString());
            FileService.writeToFile("output\\rezultati_simulacija.txt", stringBuilderIterationToFile.toString());
            ExelService exelService = new ExelService();
            exelService.writeToExel("output\\rezultati_simulacija_usrednjeno.xlsx", dataToWrite);
        } catch (IOException e) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Error", "Something happened\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void prepareForFile(LinkedList<SimulationService> simulationServiceLinkedList) {
        int iteration = 1;
        for (SimulationService simulationService : simulationServiceLinkedList) {
            stringBuilderIterationToFile.append("********************* K= ").append(inputParameters.getK())
                    .append(" ************************* N= ").append(inputParameters.getN()).append(" ************************* Iteration= ")
                    .append(iteration).append("\r\n").append(simulationServiceLinkedList.get(0).writeToFileHeader());
            iteration++;
            stringBuilderIterationToFile.append(simulationService.writeData());
            stringBuilderIterationToFile.append(String.format("%7s %,f\r\n", "TimePropagation=", simulationService.systemResponse()));
            double[] tempUsage = new double[simulationService.getSimulationServers().length];
            for (int i = 0; i < simulationService.getSimulationServers().length; i++) {
                tempUsage[i] = simulationService.getSimulationServers()[i].getUsage();
            }
            stringBuilderIterationToFile.append("Critical resource: ").append(MiscService.calculateCriticalResource(tempUsage)).append("\n");

            dataToWrite2.add(new LinkedList<>());
            dataToWrite2.getLast().add("K= " + inputParameters.getK() + ", N= " + inputParameters.getN());
            dataToWrite2.add(new LinkedList<>());
            dataToWrite2.getLast().add("usage");
            dataToWrite2.getLast().add("throughput");
            dataToWrite2.getLast().add("averageJobs");
            dataToWrite2.getLast().add("timeResponse");
            for (int i = 0; i < simulationService.getSimulationServers().length; i++) {
                dataToWrite.add(new LinkedList<>());
                dataToWrite.getLast().add(simulationService.getSimulationServers()[i].getUsage());
                dataToWrite.getLast().add(simulationService.getSimulationServers()[i].getThroughput());
                dataToWrite.getLast().add(simulationService.getSimulationServers()[i].getAverageJobs());
                dataToWrite.getLast().add(simulationService.getSimulationServers()[i].getTimeResponse());
            }
            dataToWrite.add(new LinkedList<>());
            dataToWrite.getLast().add("TimePropagation=");
            dataToWrite.getLast().add(simulationService.systemResponse());
            dataToWrite.add(new LinkedList<>());
            dataToWrite.getLast().add("Critical resource: ");
            dataToWrite.getLast().add(MiscService.calculateCriticalResource(tempUsage));
        }

        stringBuilderToFile.append("********************* K= ").append(inputParameters.getK())
                .append(" ************************* N= ").append(inputParameters.getN())
                .append(" *************************\r\n").append(simulationServiceLinkedList.get(0).writeToFileHeader());
        double[] temp = new double[simulationServiceLinkedList.get(0).getSimulationServers().length];
        double[] temp2 = new double[simulationServiceLinkedList.get(0).getSimulationServers().length];
        double[] temp3 = new double[simulationServiceLinkedList.get(0).getSimulationServers().length];
        double[] temp4 = new double[simulationServiceLinkedList.get(0).getSimulationServers().length];
        double propagation = 0;
        for (SimulationService simulationService : simulationServiceLinkedList) {
            for (int j = 0; j < simulationService.getSimulationServers().length; j++) {
                temp[j] += simulationService.getSimulationServers()[j].getUsage();
                temp2[j] += simulationService.getSimulationServers()[j].getThroughput();
                temp3[j] += simulationService.getSimulationServers()[j].getAverageJobs();
                temp4[j] += simulationService.getSimulationServers()[j].getTimeResponse();
            }
            propagation += simulationService.systemResponse();
        }
        propagation /= simulationServiceLinkedList.size();

        for (int i = 0; i < temp.length; i++) {
            temp[i] /= simulationServiceLinkedList.size();
            temp2[i] /= simulationServiceLinkedList.size();
            temp3[i] /= simulationServiceLinkedList.size();
            temp4[i] /= simulationServiceLinkedList.size();
        }

        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("K= " + inputParameters.getK() + ", N= " + inputParameters.getN());
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("usage");
        dataToWrite.getLast().add("throughput");
        dataToWrite.getLast().add("averageJobs");
        dataToWrite.getLast().add("timeResponse");

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < temp.length; i++) {
            stringBuilder.append(String.format("%,f        %,f sec-1      %,f        %,f\r\n",
                    temp[i], temp2[i], temp3[i], temp4[i]));
            dataToWrite.add(new LinkedList<>());
            dataToWrite.getLast().add(temp[i]);
            dataToWrite.getLast().add(temp2[i]);
            dataToWrite.getLast().add(temp3[i]);
            dataToWrite.getLast().add(temp4[i]);
        }
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("TimePropagation=");
        dataToWrite.getLast().add(propagation);
        dataToWrite.add(new LinkedList<>());
        dataToWrite.getLast().add("Critical resource: ");
        dataToWrite.getLast().add(MiscService.calculateCriticalResource(temp));

        stringBuilderToFile.append(stringBuilder.toString());
        stringBuilderToFile.append(String.format("%7s %,f\r\n", "TimePropagation=", propagation));
        stringBuilderToFile.append("Critical resource: ").append(MiscService.calculateCriticalResource(temp)).append("\n");
    }
}
