package com.mb160113d.prs.gui;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.services.GuiService;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class WelcomeWindow extends Stage {

    public WelcomeWindow() {
        GuiService guiService = new GuiService();
        guiService.setStage(this, "Welcome");

        FlowPane flowPane = guiService.generateFlowPane();
        Button manualImport = guiService.generateButton("Manually import");
        manualImport.setOnAction(event -> manualImportAction());

        Button analyticalSolution = guiService.generateButton("Analytical solution");
        analyticalSolution.setOnAction(event -> analyticalSolutionOnAction());

        Button simulationSolution = guiService.generateButton("Simulation solution");
        simulationSolution.setOnAction(event -> simulationSolutionOnAction());

        flowPane.getChildren().addAll(manualImport, analyticalSolution, simulationSolution);
        Scene scene = new Scene(flowPane);
        this.setScene(scene);
        this.show();
    }

    private void analyticalSolutionOnAction() {
        new SpeedWindow();
        this.close();
    }

    private void simulationSolutionOnAction() {
        new TimeSelection(new InputParameters(), false);
        this.close();
    }

    private void manualImportAction() {
        new InputK(new InputParameters());
        this.close();
    }
}
