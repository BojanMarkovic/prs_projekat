package com.mb160113d.prs.gui;

import com.mb160113d.prs.services.GuiService;
import com.mb160113d.prs.services.MiscService;
import com.mb160113d.prs.services.SimulationService;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class SimulatedWindow extends Stage {
    public SimulatedWindow(SimulationService simulationService) {
        GuiService guiService = new GuiService();
        guiService.setStage(this, "Simulated");

        FlowPane flowPane = guiService.generateFlowPane();

        try {
            simulationService.writeToFile();
        } catch (IOException e) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Error", "Something happened\n" + e.getMessage());
            e.printStackTrace();
        }

        GridPane gridPane = MiscService.generateTableForData(simulationService.getInputParameters(),
                simulationService.makeOutputParameters(), this, false);


        flowPane.getChildren().addAll(gridPane);
        this.setScene(new Scene(flowPane));
        this.show();
    }
}
