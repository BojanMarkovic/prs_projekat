package com.mb160113d.prs.gui;

import com.mb160113d.prs.exceptions.InputException;
import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.objects.Matrix;
import com.mb160113d.prs.objects.OutputParameters;
import com.mb160113d.prs.objects.TextFieldSpeeds;
import com.mb160113d.prs.services.AnalyticalService;
import com.mb160113d.prs.services.GuiService;
import com.mb160113d.prs.services.MiscService;
import com.mb160113d.prs.services.TemplateTask;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class InputWindow extends Stage {
    private static final String[] LABELS = {"Cpu", "System1", "System2", "System3", "User disc"};
    private static final double ROUNDING_CONSTANT = 10000.0;
    private static final int TEXT_FIELD_WIDTH = 70;
    private final Scene scene;
    private final GuiService guiService;
    private final InputParameters inputParameters;
    private final TextFieldSpeeds textFieldSpeeds;
    private TextField[][] textFields;
    private OutputParameters outputParameters;

    public InputWindow(InputParameters inputParameters) {
        this.inputParameters = inputParameters;
        this.textFieldSpeeds = new TextFieldSpeeds();
        this.guiService = new GuiService();
        guiService.setStage(this, "Input window");

        FlowPane flowPane = guiService.generateFlowPane();
        GridPane gridPane = generateMatrix();

        Button analyticalMethod = guiService.generateButton("Analytical method");
        analyticalMethod.setOnAction(event -> analyticalMethodOnAction());

        Button simulationMethod = guiService.generateButton("Simulate");
        simulationMethod.setOnAction(event -> simulationMethodOnAction());

        Button back = guiService.generateButton("Back");
        back.setOnAction(event -> {
            new InputK(inputParameters);
            this.close();
        });
        HBox buttonsHBox = new HBox(analyticalMethod, simulationMethod, back);

        GridPane gridPaneHBox = MiscService.generateSpeedInput(buttonsHBox, textFieldSpeeds, inputParameters, true);

        flowPane.getChildren().addAll(gridPane, gridPaneHBox);
        this.scene = new Scene(flowPane);
        this.setScene(scene);
        this.show();
    }

    private void simulationMethodOnAction() {
        try {
            getInputParameters();
            new TimeSelection(inputParameters, true);
            this.close();
        } catch (NumberFormatException exception) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Input not valid", "Everything must be number (int or double)");
        } catch (InputException e) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Input not valid", e.getMessage());
        }
    }

    private void analyticalMethodOnAction() {
        try {
            getInputParameters();
            TemplateTask templateTask = new TemplateTask(scene, this, () -> {
                outputParameters = (new AnalyticalService()).generateOutput(inputParameters);
                return null;
            }, () -> {
                new ProcessedWindow(outputParameters, inputParameters);
                return null;
            });
            guiService.getProgressIndicator(this).progressProperty().bind(templateTask.progressProperty());
            Thread thread = new Thread(templateTask);
            thread.setDaemon(true);
            thread.start();
        } catch (NumberFormatException exception) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Input not valid", "Everything must be number (int or double)");
        } catch (InputException e) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Input not valid", e.getMessage());
        }
    }

    private void getInputParameters() throws InputException {
        inputParameters.setSpeeds(textFieldSpeeds);
        double[][] matrix = new double[inputParameters.getSize()][inputParameters.getSize()];
        for (int i = 0; i < inputParameters.getSize(); i++) {
            double sum = 0;
            for (int j = 0; j < inputParameters.getSize(); j++) {
                matrix[i][j] = Double.parseDouble(textFields[i][j].getText());
                sum += Math.round(matrix[i][j] * ROUNDING_CONSTANT) / ROUNDING_CONSTANT;
            }
            sum = Math.round(sum * ROUNDING_CONSTANT) / ROUNDING_CONSTANT;
            if (sum != 1) {
                throw new InputException("In each row of matrix sum must be 1");
            }
            if (matrix[Matrix.CPU][Matrix.CPU] == 0) {
                throw new InputException("matrix[0][0] must not be 0");
            }
        }
        inputParameters.setMatrix(new Matrix(matrix));
    }

    private GridPane generateMatrix() {
        textFields = new TextField[inputParameters.getSize()][inputParameters.getSize()];
        GridPane gridPane = new GridPane();
        for (int i = 0; i < inputParameters.getSize(); i++) {
            if (i < 4) {
                gridPane.add(guiService.generateLabel(LABELS[i]), i + 1, 0);
                gridPane.add(guiService.generateLabel(LABELS[i]), 0, i + 1);
            } else {
                gridPane.add(guiService.generateLabel(LABELS[Matrix.K] + (i - 3)), i + 1, 0);
                gridPane.add(guiService.generateLabel(LABELS[Matrix.K] + (i - 3)), 0, i + 1);
            }
            for (int j = 0; j < inputParameters.getSize(); j++) {
                textFields[i][j] = guiService.generateTextField(String.valueOf(inputParameters.getMatrix().getMatrix()[i][j]));
                textFields[i][j].setPrefWidth(TEXT_FIELD_WIDTH);
                gridPane.add(textFields[i][j], j + 1, i + 1);
            }
        }
        return gridPane;
    }
}
