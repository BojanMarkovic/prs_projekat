package com.mb160113d.prs.gui;

import com.mb160113d.prs.objects.InputParameters;
import com.mb160113d.prs.objects.OutputParameters;
import com.mb160113d.prs.services.GuiService;
import com.mb160113d.prs.services.MiscService;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ProcessedWindow extends Stage {
    public ProcessedWindow(OutputParameters outputParameters, InputParameters inputParameters) {
        GuiService guiService = new GuiService();
        guiService.setStage(this, "Processed");

        FlowPane flowPane = guiService.generateFlowPane();

        try {
            outputParameters.writeToFile();
        } catch (IOException e) {
            guiService.generateAlertWindow(Alert.AlertType.ERROR, "Error", "Something happened\n" + e.getMessage());
            e.printStackTrace();
        }

        GridPane gridPane = MiscService.generateTableForData(inputParameters, outputParameters, this, true);

        flowPane.getChildren().addAll(gridPane);
        this.setScene(new Scene(flowPane));
        this.show();
    }
}
